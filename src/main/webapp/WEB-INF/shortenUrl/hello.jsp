<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="kr">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.0.js"></script>
    
    
</head>
<body>


    <div>
        변경 할 UR7L (http:// | https:// 포함) : <input id="url_input" text="" />
        <input type="button" onclick="convert();" value="변경"/>
    </div>
    <div id="result" style="max-width: 1024px;"></div>
</body>
</html>







<script type="text/javascript">

    //단일 데이터 메칭
    function convert(){
        var text = $("#url_input").val();
        if(text != null){
		
            $.ajax({
                type: "get",
                url: "/covertUrl",
                data: {urlStr: text},
                success: function(data) {
                	console.log("test");
                    console.log(data.ShortenUrl);
                    console.log(data.OrginUrL);
                    console.log(data.count);
                    console.log(data);
                    
                    if(data.successFlag){
                        if(data.shortUrlType == "ORIGIN"){
                            $("#result").html("<p>원본 URL : " + data.OrginUrL +"</p><p> 요청 횟수:" + data.count + "</p>");
                        }else{
                            $("#result").html("<p>변경 URL : " + data.ShortenUrl +"</p><p> 요청 횟수:" + data.count + "</p>");
                        }

                    }else{
                        $("#result").html("<p>"+text + ' 줄이기 실패. </p>');
                    }

                }
            });

        }
    }
</script>