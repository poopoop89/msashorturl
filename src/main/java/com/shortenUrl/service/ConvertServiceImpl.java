package com.shortenUrl.service;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shortenUrl.Util.UrlEncoder;
 
@Service("urlConvertService")
@Transactional 
public class ConvertServiceImpl implements ConvertService {
	 @Autowired
    private  UrlEncoder urlEncoder;
    private static  ArrayList<HashMap<String, String>> shorUrllist = new ArrayList<HashMap<String,String>>();

	@Override
	public HashMap  getShortenUrl(String url) {
		// TODO Auto-generated method stub
		System.out.println("call Service");
		HashMap paramMap = new HashMap();
		//url 체크 
		if(url.isEmpty()) {
			return null;
		}
		try {
			boolean dupUrl=false;
			int index=0;
			for(index=0;index<shorUrllist.size();index++) {
				if(url.equals(shorUrllist.get(index).get("OrginUrL"))
				  ) {
					dupUrl=true;
					paramMap.put("shortUrlType", "SHORTEN"); //JSP에는 ORGIN URL에 대한 SHOTRNE URL 을 리턴
					break;
				}else if (url.equals(shorUrllist.get(index).get("ShortenUrl"))){
					dupUrl=true;
					paramMap.put("shortUrlType", "ORIGIN"); //JSP에는 SHORTEN URL에 대한 ORGIN URL 을 리턴
					break;
				}
			}
			
			//기등록된건에 대하여 count+1 후 url리턴
			if(dupUrl) {
				//count+1
				String count =String.valueOf(shorUrllist.get(index).get("count"));
				int cnt = Integer.parseInt(count);
				shorUrllist.get(index).put("count",String.valueOf(++cnt));
				
				
				paramMap.put("OrginUrL"	 ,shorUrllist.get(index).get("OrginUrL"));
				paramMap.put("ShortenUrl", shorUrllist.get(index).get("ShortenUrl"));
				paramMap.put("count" 	 , cnt);
				paramMap.put("successFlag", true);
				
			//신규생성
			}else {
				int seq=shorUrllist.size()+1;// url호출 횟수
				String encoder =urlEncoder.urlEncoder(Integer.toString(seq));
				System.out.println(encoder);
				
				paramMap.put("OrginUrL", url);
				paramMap.put("ShortenUrl", encoder);
				paramMap.put("count", 1);
				paramMap.put("successFlag", true);
				shorUrllist.add(paramMap);
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			paramMap.put("successFlag", false);
			e.printStackTrace();
			
		}
		System.out.println(paramMap);
		return paramMap;
	}
	  // Encode Sequence
    private String encodingUrl(String seqStr) throws Exception{
        return urlEncoder.urlEncoder(seqStr);
    }
}
