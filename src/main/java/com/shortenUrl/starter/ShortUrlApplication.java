package com.shortenUrl.starter;
 
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages= {"com.shortenUrl"})
public class ShortUrlApplication {

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(ShortUrlApplication.class);
		application.setWebApplicationType(WebApplicationType.SERVLET);
		application.run(args);
	}

}
