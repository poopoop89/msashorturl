package com.shortenUrl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

public class ConvertController {

	public ConvertController() {
		System.out.println(this.getClass()+"생성");
	}
 	
	@RequestMapping("/hello")
	public String hello(Model model) {
		model.addAttribute("hello","tees");
		return "/hello";
	}
	
}
