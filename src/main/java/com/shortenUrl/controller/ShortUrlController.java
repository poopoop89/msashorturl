package com.shortenUrl.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shortenUrl.service.ConvertServiceImpl;

@Controller
 
public class ShortUrlController {
	
	  @Autowired
	  private ConvertServiceImpl urlConvertService;
	/*	
	private final ConvertServiceImpl urlConvertService;

    @Autowired
    public ShortUrlController(ConvertServiceImpl urlConvertService){
        this.urlConvertService = urlConvertService;
    }
    */
	@GetMapping(value="/covertUrl",produces= {"application/json"})
	@ResponseBody
	public HashMap  covertUrl(@RequestParam(defaultValue="") String urlStr) {
		System.out.println(urlStr+"<--"); 
		
		return urlConvertService.getShortenUrl(urlStr);
	}
	
}
